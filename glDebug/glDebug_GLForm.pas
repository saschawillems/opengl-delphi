{
	This code is licensed under the Mozilla Public License Version 2.0 (http://opensource.org/licenses/MPL-2.0)
	� 2014 by Sascha Willems - www.saschawillems.de
}

unit glDebug_GLForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, dglOpenGL, Vcl.ExtCtrls, Vcl.StdCtrls;

type
  TGLForm = class(TForm)
    StartRenderLoopTimer: TTimer;
    GLPanel: TPanel;
    GLLog: TMemo;
    PanelButtons: TPanel;
    ButtonLegacyGL: TButton;
    ButtonInvalid: TButton;
    ButtonPerformance: TButton;
    procedure FormCreate(Sender: TObject);
    procedure StartRenderLoopTimerTimer(Sender: TObject);

    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ButtonLegacyGLClick(Sender: TObject);
    procedure ButtonInvalidClick(Sender: TObject);
    procedure ButtonPerformanceClick(Sender: TObject);
  private
    { Private-Deklarationen }
    RC : HGLRC;
    DC : HDC;

    TriangleVBO : array[0..2] of TGLUInt;
    TriangleVA  : array[0..1] of TGLUInt;

    ProgramObject : TGLUInt;

		Quit: Boolean;

    procedure RenderFrame;

		function CreateRenderingContext : HGLRC;
    procedure SetupOpenGL;

    function GetInfoLog(glObject : glHandle) : String;
		procedure LoadShaders(pVShaderFile, pFShaderFile : String);
  end;

var
  GLForm: TGLForm;

implementation

{$R *.dfm}

///	<summary>
///	  This function will be called whenever debug messages
///		are sent by the OpenGL implementation
///	</summary>
procedure GLDebugCallBack(source : GLEnum; type_ : GLEnum; id : GLUInt; severity : GLUInt; length : GLsizei; const message_ : PGLCHar; userParam : PGLvoid); stdcall;
var
	MsgSource   : String;
  MsgType     : String;
  MsgSeverity : String;
begin

	// Source of this message
  case source of
    GL_DEBUG_SOURCE_API              : MsgSource := 'API';
    GL_DEBUG_SOURCE_WINDOW_SYSTEM    : MsgSource := 'WINDOW_SYSTEM';
    GL_DEBUG_SOURCE_SHADER_COMPILER  : MsgSource := 'SHADER_COMPILER';
    GL_DEBUG_SOURCE_THIRD_PARTY      : MsgSource := 'THIRD_PARTY';
    GL_DEBUG_SOURCE_APPLICATION      : MsgSource := 'APPLICATION';
    GL_DEBUG_SOURCE_OTHER            : MsgSource := 'OTHER';
  end;

  // Type of this message
  case type_ of
    GL_DEBUG_TYPE_ERROR               : MsgType := 'ERROR';
    GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR : MsgType := 'DEPRECATED_BEHAVIOR';
    GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR  : MsgType := 'UNDEFINED_BEHAVIOR';
    GL_DEBUG_TYPE_PORTABILITY         : MsgType := 'PORTABILITY';
    GL_DEBUG_TYPE_PERFORMANCE         : MsgType := 'PERFORMANCE';
    GL_DEBUG_TYPE_OTHER               : MsgType := 'OTHER';
  end;

  // Severity of this message
  case severity of
     GL_DEBUG_SEVERITY_HIGH           : MsgSeverity := 'HIGH';
     GL_DEBUG_SEVERITY_MEDIUM 			  : MsgSeverity := 'MEDIUM';
     GL_DEBUG_SEVERITY_LOW    			  : MsgSeverity := 'LOW';
  end;

  GLForm.GLLog.Lines.Add(Format('%s %s [%s] : %s', [MsgSource, MsgType, MsgSeverity, message_]));
end;


procedure TGLForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	Quit := True;
end;

procedure TGLForm.FormCreate(Sender: TObject);
begin
	SetupOpenGL;
end;

function TGLForm.GetInfoLog(glObject : glHandle) : String;
var
  blen,slen : GLInt;
  InfoLog   : PGLChar;
begin
  glGetShaderiv(glObject, GL_INFO_LOG_LENGTH , @blen);
  if blen > 1 then
    begin
      GetMem(InfoLog, blen*SizeOf(PGLChar));
      glGetShaderInfoLog(glObject, blen, @slen, InfoLog);
      Result := InfoLog;
      Dispose(InfoLog);
    end;
end;

procedure TGLForm.ButtonInvalidClick(Sender: TObject);
begin
	// Call OpenGL function with invalid identifier
  glEnable(GL_MODELVIEW);
  glPolygonMode(GL_FRONT, GL_LINE);
  glEnable(GL_POINT_SMOOTH);
end;

procedure TGLForm.ButtonLegacyGLClick(Sender: TObject);
begin
	// Run some deprecated legacy functionality that will cause debug messages

 	glMatrixMode(GL_PROJECTION);
  gluPerspective(60, ClientWidth/ClientHeight, 0.1, 128);
  glLoadIdentity;

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity;

  glBegin(GL_POINTS);
  	glColor3f(0, 0, 1); glVertex3f(1, 1, 0);
  glEnd;

end;

procedure TGLForm.ButtonPerformanceClick(Sender: TObject);
var
	ID : TGLUInt;
  DepthID : TGLUInt;
  TexID : TGLUInt;
  Status : TGLUInt;
  Width : Integer;
  Height : Integer;
begin
	// Generate a framebuffer object that may generate a performance warning
  // Note : Performance warnings depend heavily on IHV, so you may not see
  //        this warning on e.g. NVidia

  Width := 512;
  Height := 512;

  glGenFramebuffers(1, @ID);
  glBindFramebuffer(GL_FRAMEBUFFER, ID);

  // Depth
  glGenRenderbuffers(1, @DepthID);
  glBindRenderbuffer(GL_RENDERBUFFER, DepthID);
  glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT32, Width, Height);
  glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, DepthID);

  // Texture
  glGenTextures(1, @TexID);
  glBindTexture(GL_TEXTURE_2D, TexID);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB10_A2, Width, Height, 0, GL_RGBA, GL_UNSIGNED_BYTE, nil);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
  glGenerateMipmapEXT(GL_TEXTURE_2D);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, TexID, 0);

 	Status := glCheckFramebufferStatus(GL_FRAMEBUFFER);

  // This will generate an API performance warning on current ATI drivers
  glBindFramebuffer(GL_FRAMEBUFFER, ID);
  glReadBuffer(GL_COLOR_ATTACHMENT3);
  glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
  glBlitFramebuffer(0, 0, width, height, 0, 0, width, height, GL_COLOR_BUFFER_BIT, GL_NEAREST);

  glDeleteFramebuffersEXT(1, @ID);
  glDeleteRenderbuffersEXT(1, @DepthID);
  glDeleteTextures(1, @TexID);
end;

function TGLForm.CreateRenderingContext;
const
  PFD_DOUBLEBUFFER   = $00000001;
  PFD_DRAW_TO_WINDOW = $00000004;
  PFD_SUPPORT_OPENGL = $00000020;
var
  PFDescriptor : TPixelFormatDescriptor;
  PixelFormat  : Integer;
  AType        : DWORD;
  LegacyRC     : HGLRC;
  Attribs      : array of Integer;
begin
 	InitOpenGL;

  FillChar(PFDescriptor, SizeOf(PFDescriptor), 0);

  with PFDescriptor do
    begin
      nSize    := SizeOf(PFDescriptor);
      nVersion := 1;
      dwFlags  := PFD_SUPPORT_OPENGL or PFD_DRAW_TO_WINDOW or PFD_DOUBLEBUFFER;
      AType    := GetObjectType(DC);

      if AType = 0 then
      	RaiseLastOSError;

    	dwFlags := dwFlags ;

      iPixelType   := 0;
      cColorBits   := 32;
      cDepthBits   := 24;
      cStencilBits := 8;
      cAccumBits   := 0;
      cAuxBuffers  := 0;

      iLayerType   := 0
    end;

  PixelFormat := ChoosePixelFormat(DC, @PFDescriptor);

  if PixelFormat = 0 then
  	RaiseLastOSError;

  if GetPixelFormat(DC) <> PixelFormat then
  	if not SetPixelFormat(DC, PixelFormat, @PFDescriptor) then
  		RaiseLastOSError;

  DescribePixelFormat(DC, PixelFormat, SizeOf(PFDescriptor), PFDescriptor);

  // Create legacy render context first for we need function pointers to
  // create new OpenGL render contexts
  LegacyRC := wglCreateContext(DC);
  wglMakeCurrent(DC, LegacyRC);

  // Set attributes to describe our requested context
  SetLength(Attribs, 7);
  // OpenGL debug render context is available with OpenGL 4.3 and up
  Attribs[0] := WGL_CONTEXT_MAJOR_VERSION_ARB;
  Attribs[1] := 4;
  Attribs[2] := WGL_CONTEXT_MINOR_VERSION_ARB;
  Attribs[3] := 3;
  // Now set the flags to create a forward compatible context with debug capabilities
  Attribs[4] := WGL_CONTEXT_FLAGS_ARB;
  Attribs[5] := WGL_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB or WGL_CONTEXT_DEBUG_BIT_ARB;
	// Attribute flags must be finalized with a zero
  Attribs[6] := 0;

  // Get function pointer for new context creation function
  wglCreateContextAttribsARB := wglGetProcAddress('wglCreateContextAttribsARB');

  if not Assigned(wglCreateContextAttribsARB) then
    begin
      raise Exception.Create('Could not get function pointer adress for wglCreateContextAttribsARB - OpenGL 3.x and above not supported!');
      wglDeleteContext(LegacyRC);
      exit;
    end;

  // Create context
  Result := wglCreateContextAttribsARB(DC, 0, @Attribs[0]);

  if Result = 0 then
    begin
      raise Exception.Create('Could not create the desired OpenGL rendering context!' + sLineBreak + 'OpenGL 4.3 or up is required!');
      wglDeleteContext(LegacyRC);
      exit;
    end;

  wglDeleteContext(LegacyRC);

  if Result = 0 then
  	RaiseLastOSError
  else
  	LastPixelFormat := 0;
end;

procedure TGLForm.LoadShaders(pVShaderFile, pFShaderFile : String);
var
  VShader : glHandle;
  FShader : glHandle;
  SSource : TStringList;
  Source  : AnsiString;
  InfoLog : String;
  SLen    : Integer;
begin
  ProgramObject  := glCreateProgram;
  VShader        := glCreateShader(GL_VERTEX_SHADER);
  FShader        := glCreateShader(GL_FRAGMENT_SHADER);
  SSource        := TStringList.Create;
  SSource.LoadFromFile(pVShaderFile);
  Source := SSource.Text;
  SLen   := Length(Source);
  glShaderSource(VShader, 1, @Source, @SLen);
  InfoLog := GetInfoLog(VShader);
  if InfoLog <> '' then
  	OutPutDebugString(PChar('Vertexshader : '+InfoLog));

  SSource.LoadFromFile(pFShaderFile);
  Source := SSource.Text;
  SLen   := Length(Source);
  glShaderSource(FShader, 1, @Source, @SLen);
  InfoLog := GetInfoLog(FShader);
  if InfoLog <> '' then
  	OutPutDebugString(PChar('Fragmentshader : '+InfoLog));

  SSource.Free;

  glCompileShader(VShader);
  InfoLog := GetInfoLog(VShader);
  if InfoLog <> '' then
  	OutPutDebugString(PChar('Vertexshader : '+InfoLog));

  glCompileShader(FShader);
  InfoLog := GetInfoLog(FShader);
  if InfoLog <> '' then
  	OutPutDebugString(PChar('Fragmentshader : '+InfoLog));

  glAttachShader(ProgramObject, VShader);
  glAttachShader(ProgramObject, FShader);
  glDeleteShader(VShader);
  glDeleteShader(FShader);

  glBindAttribLocation(ProgramObject, 0, 'in_Position');
  glBindAttribLocation(ProgramObject, 1, 'in_Color');

  glLinkProgram(ProgramObject);

  glUseProgram(ProgramObject);
end;

procedure TGLForm.RenderFrame;
begin
  glViewport(0, 0, ClientWidth, ClientHeight);
  glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);

  glDrawArrays(GL_TRIANGLES, 0, 3);

  SwapBuffers(DC);
end;

procedure TGLForm.SetupOpenGL;
var
 TriVerts  : array[0..9] of TGLFloat;
 TriCols   : array[0..9] of TGLFloat;
begin

  // Setup OpenGL
  InitOpenGL;

  DC := GetDC(GLPanel.Handle);
  RC := CreateRenderingContext;

  ActivateRenderingContext(DC, RC);

  glEnable(GL_DEBUG_OUTPUT);

  GLLog.Lines.Add('GL_RENDERER = '+ glGetString(GL_RENDERER));
  GLLog.Lines.Add('GL_VERSION = '+ glGetString(GL_VERSION));

  glClearColor(0, 0, 0.4, 0);

  glEnable(GL_DEPTH_TEST);
  glClearColor(0, 0, 0.4, 0);
  glDepthFunc(GL_LEQUAL);
  // Create vertex array data for triangle;

  TriVerts[0] :=  0.0; TriVerts[1] :=  0.5; TriVerts[2] := 0;
  TriVerts[3] := -0.5; TriVerts[4] := -0.5; TriVerts[5] := 0;
  TriVerts[6] :=  0.5; TriVerts[7] := -0.5; TriVerts[8] := 0;

  TriCols[0]  := 1;    TriCols[1]  := 0;    TriCols[2]  := 0;
  TriCols[3]  := 0;    TriCols[4]  := 1;    TriCols[5]  := 0;
  TriCols[6]  := 0;    TriCols[7]  := 0;    TriCols[8]  := 1;

  glGenVertexArrays(2, @TriangleVA[0]);

  glBindVertexArray(TriangleVA[0]);
  glGenBuffers(2, @TriangleVBO[0]);

  glBindBuffer(GL_ARRAY_BUFFER, TriangleVBO[0]);
  glBufferData(GL_ARRAY_BUFFER, 9*SizeOf(TGLFloat), @TriVerts[0], GL_STATIC_DRAW);
  glVertexAttribPointer(0, 3, GL_FLOAT, False, 0, nil);
  glEnableVertexAttribArray(0);

  glBindBuffer(GL_ARRAY_BUFFER, TriangleVBO[1]);
  glBufferData(GL_ARRAY_BUFFER, 9*SizeOf(TGLFloat), @TriCols[0], GL_STATIC_DRAW);
  glVertexAttribPointer(1, 3, GL_FLOAT, False, 0, nil);
  glEnableVertexAttribArray(1);

  LoadShaders('data\shader\vertex.shader', 'data\shader\fragment.shader');

  // Assign our message event to the gl debug callback
  glDebugMessageCallback(@GLDebugCallBack, nil);
	// And tell the driver to give us all available GL debug messages, see http://davidgow.net/musings/nvidia-opengl-debug-output.html
  glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nil, True);

  // Initiate render loop
  StartRenderLoopTimer.Enabled := True;
end;

procedure TGLForm.StartRenderLoopTimerTimer(Sender: TObject);
begin
  StartRenderLoopTimer.Enabled := False;
  repeat
    RenderFrame;
    Application.ProcessMessages;
  until Quit;
  DeactivateRenderingContext;
  DestroyRenderingContext(RC);
  ReleaseDC(DC, GLPanel.Handle);
end;

end.
