program glDebug;

uses
  Vcl.Forms,
  glDebug_GLForm in 'glDebug_GLForm.pas' {GLForm};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TGLForm, GLForm);
  Application.Run;
end.
