#version 140

in vec3 in_Position;
in vec3 in_Color;
uniform float timer;

varying vec3 ex_Color;

void main(void)
{
    mat3 rotation = mat3(
        vec3( cos(timer),  sin(timer),  0.0),
        vec3(-sin(timer),  cos(timer),  0.0),
        vec3(        0.0,         0.0,  1.0)
    );

    gl_Position = vec4(rotation * in_Position.xyz, 1.0);

    ex_Color = in_Color;

}