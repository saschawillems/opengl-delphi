object GLForm: TGLForm
  Left = 0
  Top = 0
  Caption = 'OpenGL Debug Demo'
  ClientHeight = 707
  ClientWidth = 806
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object GLPanel: TPanel
    Left = 0
    Top = 0
    Width = 806
    Height = 495
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitTop = -6
    ExplicitHeight = 480
  end
  object GLLog: TMemo
    Left = 0
    Top = 536
    Width = 806
    Height = 171
    Align = alBottom
    Color = clBlack
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    ScrollBars = ssVertical
    TabOrder = 1
  end
  object PanelButtons: TPanel
    Left = 0
    Top = 495
    Width = 806
    Height = 41
    Align = alBottom
    TabOrder = 2
    ExplicitLeft = 320
    ExplicitTop = 280
    ExplicitWidth = 185
    object ButtonLegacyGL: TButton
      Left = 8
      Top = 10
      Width = 75
      Height = 25
      Caption = 'Legacy GL'
      TabOrder = 0
      OnClick = ButtonLegacyGLClick
    end
    object ButtonInvalid: TButton
      Left = 89
      Top = 10
      Width = 75
      Height = 25
      Caption = 'Invalid cmd'
      TabOrder = 1
      OnClick = ButtonInvalidClick
    end
    object ButtonPerformance: TButton
      Left = 170
      Top = 10
      Width = 75
      Height = 25
      Caption = 'Performance'
      TabOrder = 2
      OnClick = ButtonPerformanceClick
    end
  end
  object StartRenderLoopTimer: TTimer
    Enabled = False
    Interval = 100
    OnTimer = StartRenderLoopTimerTimer
    Left = 40
    Top = 32
  end
end
