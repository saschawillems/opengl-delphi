program ComputeShaderParticleSystem;

uses
  Vcl.Forms,
  ComputeShaderParticleSystem_GLForm in 'ComputeShaderParticleSystem_GLForm.pas' {GLForm};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TGLForm, GLForm);
  Application.Run;
end.
