{
	This code is licensed under the Mozilla Public License Version 2.0 (http://opensource.org/licenses/MPL-2.0)
	� 2014 by Sascha Willems - www.saschawillems.de
}

unit ComputeShaderParticleSystem_GLForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, dglOpenGL, Vcl.ExtCtrls, Math, Vcl.Menus;

const
	FORM_AFTER_SHOW = WM_USER + 300;

type
	TGLVector4f = packed record
		x,y,z,w : Single;
  end;

  PGLVectorArray4f = ^TGLVectorArray4f;
  TGLVectorArray4f = packed array[0..0] of TGLVector4f;

  TGLFormOptions = record
    ClearColorBuffer : Boolean;
    ParticleClamp    : Boolean;
  end;

  TGLForm = class(TForm)
    FPSTimer: TTimer;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FPSTimerTimer(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure FormShow(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private-Deklarationen }
    DC: HDC;
    RC: HGLRC;

    Quit : Boolean;

    Options : TGLFormOptions;

    ComputeProgHandle : TGLUInt;

    RenderProgHandle : TGLUInt;

    QPCf : Int64;

  	FrameDelta: Single;
    FrameColor : TGLVector3f;
    FrameColIndex : Integer;
    FrameColDir : SmallInt;

    SSBOPosition : TGLUInt;
    SSBOVelocity : TGLUInt;

    FrameCount : Integer;

    procedure Init;

    procedure RenderScene;
    procedure RenderLoop;

    function GenerateRenderPrograms : TGLUInt;

    procedure FillPositionSSBO(ABufferPointer : PGLVectorArray4f; AParticleCount : Integer);
    procedure FillVelocitySSBO(ABufferPointer : PGLVectorArray4f; AParticleCount : Integer);
    procedure GenerateSSBO(AParticleCount : Integer);

    procedure ResetBuffers(AParticleCount : Integer);

    procedure AfterShow(var Msg: TMessage); message FORM_AFTER_SHOW;

  public
    { Public-Deklarationen }
  end;

var
	Pause : Boolean;
  GLForm: TGLForm;
	ParticleCount : Integer;
  MPos : TPoint;

implementation

{$R *.dfm}

procedure TGLForm.AfterShow(var Msg: TMessage);
begin
	Options.ClearColorBuffer := True;
  Options.ParticleClamp		 := True;
	RenderLoop;
end;

procedure TGLForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	Quit := True;
end;

function GetInfoLog(glObject : glHandle) : String;
var
 blen : GLInt;
 slen : GLSizei;
 InfoLog   : PGLChar;
begin
  glGetShaderiv(glObject, GL_INFO_LOG_LENGTH , @blen);
  if blen > 1 then
    begin
      GetMem(InfoLog, blen*SizeOf(PGLCharARB));
      glGetShaderInfoLog(glObject, blen, @slen, InfoLog);
      Result := PAnsiChar(InfoLog);
      Dispose(InfoLog);
    end;
end;

function GetProgramInfoLog(glObject : glHandle) : String;
var
 blen : GLInt;
 slen : GLsizei;
 InfoLog   : PGLCharARB;
begin
  glGetProgramiv(glObject, GL_INFO_LOG_LENGTH , @blen);
  if blen > 1 then
    begin
      GetMem(InfoLog, blen*SizeOf(PGLCharARB));
      glGetProgramInfoLog(glObject, blen, @slen, InfoLog);
      Result := PAnsiChar(InfoLog);
      Dispose(InfoLog);
    end;
end;

procedure TGLForm.FormCreate(Sender: TObject);
begin
	Init;
  Quit := False;
end;

procedure TGLForm.FormDestroy(Sender: TObject);
begin
	DeactivateRenderingContext;
  ReleaseDC(Handle, DC);
  DestroyRenderingContext(RC);
end;

procedure TGLForm.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
	if Key = VK_F11 then
  	if WindowState = wsMaximized then
    	begin
        WindowState := wsNormal;
        BorderStyle	:= bsSizeable;
      end
    else
    	begin
        BorderStyle	:= bsNone;
        WindowState := wsMaximized;
      end;

  if Key = VK_ESCAPE then
  	Close;
end;

procedure TGLForm.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = 'r' then
  	ResetBuffers(ParticleCount);
  if Key = 'p' then
  	Pause := not Pause;
  if Key = 'c' then
  	Options.ClearColorBuffer := not Options.ClearColorBuffer;
  if Key = 'b' then
  	Options.ParticleClamp := not Options.ParticleClamp;
end;

procedure TGLForm.FormMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin
	MPos := Point(X - ClientWidth div 2, ClientHeight div 2 - Y);
end;

procedure TGLForm.FormShow(Sender: TObject);
begin
	PostMessage(Self.Handle, FORM_AFTER_SHOW, 0, 0);
end;

procedure TGLForm.FPSTimerTimer(Sender: TObject);
begin
	Caption    := 'OpenGL Compute shader sample - ' + IntToStr(ParticleCount) + ' particles @ ' + IntToStr(Round(FrameCount)) + ' fps';
  FrameCount := 0;
end;

function TGLForm.GenerateRenderPrograms: TGLUInt;
const

	VertexProgram : AnsiString =
		' in vec2 pos;' + SLineBreak +
		' void main() {' + SLineBreak +
		'	 gl_Position = vec4(pos.x, pos.y, 0.0, 1.0);' + SLineBreak +
		' }';

  FragmentProgram : AnsiString =
		'uniform sampler2D srcTex;' + SLineBreak +
    'uniform vec3 inColor;' + sLineBreak +
		'out vec4 color;' + SLineBreak +
		'void main() {' + SLineBreak +
		' color = vec4(inColor.rgb, 0.25);' + SLineBreak +
		'}';

var
	VP, FP : TGLUInt;
  SLen: Integer;
  RValue : TGLUInt;
  ShaderLog : String;
begin
	Result := glCreateProgram;

	VP := glCreateShader(GL_VERTEX_SHADER);
  FP := glCreateShader(GL_FRAGMENT_SHADER);

  SLen := Length(VertexProgram);
  glShaderSource(VP, 1, @VertexProgram, @SLen);

  SLen := Length(FragmentProgram);
  glShaderSource(FP, 1, @FragmentProgram, @SLen);


  glCompileShader(VP);
  glGetShaderiv(VP, GL_COMPILE_STATUS, @RValue);
  if RValue = GL_FALSE then
  	begin
    	ShaderLog := GetInfoLog(VP);
			MessageDlg(ShaderLog, mtError, [mbOK], 0);
      exit;
    end;
  glAttachShader(Result, VP);

  glCompileShader(FP);
  glGetShaderiv(FP, GL_COMPILE_STATUS, @Rvalue);
  if RValue = GL_FALSE then
  	begin
    	ShaderLog := GetInfoLog(FP);
			MessageDlg(ShaderLog, mtError, [mbOK], 0);
      exit;
    end;
  glAttachShader(Result, FP);

	glBindFragDataLocation(Result, 0, 'color');
  glLinkProgram(Result);

  glGetProgramiv(Result, GL_LINK_STATUS, @Rvalue);
  if RValue = GL_FALSE then
  	begin
    	ShaderLog := GetProgramInfoLog(Result);
			MessageDlg(ShaderLog, mtError, [mbOK], 0);
      exit;
    end;

	glUseProgram(Result);
end;

procedure TGLForm.FillPositionSSBO(ABufferPointer : PGLVectorArray4f; AParticleCount: Integer);
var
  i : Integer;
  RndVal: Single;
  RndRad: Single;
begin
	// Generate position vectors with random spherical distribution
  for i := 0 to AParticleCount - 1 do begin
    RndVal := Random * 360;
    RndRad := Random * 0.25;

    ABufferPointer^[i].x := Cos(DegToRad(RndVal)) * RndRad;
    ABufferPointer^[i].y := Sin(DegToRad(RndVal)) * RndRad;
    ABufferPointer^[i].z := 0;
    ABufferPointer^[i].w := 1;
  end;
end;

procedure TGLForm.FillVelocitySSBO(ABufferPointer : PGLVectorArray4f; AParticleCount: Integer);
var
	i : Integer;
begin
	// All perticles start out with no velocity
  for i := 0 to AParticleCount - 1 do begin
    ABufferPointer^[i].x := 0;
    ABufferPointer^[i].y := 0;
    ABufferPointer^[i].z := 0;
    ABufferPointer^[i].w := 1;
  end;
end;

// Generate and fill our position and velocity shader storage buffer object
//  Position SSBO bound to target index 0
//  Velocity SSBO bound to target index 1
procedure TGLForm.GenerateSSBO(AParticleCount : Integer);
var
	BufferPointer : PGLVectorArray4f;
  i : Integer;
begin
	// Position SSBO
  // Stores initial particle positions

	// Generate buffer object
  glGenBuffers(1, @SSBOPosition);

  // Bind to SSBO
  glBindBuffer(GL_SHADER_STORAGE_BUFFER, SSBOPosition);

  // Generate empty storage for the SSBO
  glBufferData(GL_SHADER_STORAGE_BUFFER, AParticleCount * SizeOf(TGLVector4f), nil, GL_STATIC_DRAW);

  // Map buffer
  BufferPointer := glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, AParticleCount * SizeOf(TGLVector4f), GL_MAP_WRITE_BIT or GL_MAP_INVALIDATE_BUFFER_BIT);

  FillPositionSSBO(BufferPointer, AParticleCount);

	glUnMapBuffer(GL_SHADER_STORAGE_BUFFER);

  // Bind buffer to target index 0
  glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, SSBOPosition);

  // Velocity SSBO
  // Stores randomized velocity vectors for the particles

	// Generate buffer object
  glGenBuffers(1, @SSBOVelocity);

  // Bind to SSBO
  glBindBuffer(GL_SHADER_STORAGE_BUFFER, SSBOVelocity);

  // Generate empty storage for the SSBO
  glBufferData(GL_SHADER_STORAGE_BUFFER, AParticleCount * SizeOf(TGLVector4f), nil, GL_STATIC_DRAW);

  // Map buffer
  BufferPointer := glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, AParticleCount * SizeOf(TGLVector4f), GL_MAP_WRITE_BIT or GL_MAP_INVALIDATE_BUFFER_BIT);

  FillVelocitySSBO(BufferPointer, AParticleCount);

	glUnMapBuffer(GL_SHADER_STORAGE_BUFFER);

  // Bind buffer to target index 0
  glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, SSBOVelocity);
end;

procedure TGLForm.Init;
var
  SLen: Integer;
  Rvalue: Integer;
  ShaderLog: string;
  ComputeShaderSrc : AnsiString;
  ComputeShader : TGLUInt;
begin
	Randomize;

  DC := GetDC(Handle);
  RC := CreateRenderingContext(DC, [opDoubleBuffered], 32, 24, 8, 0, 0, 0);
  ActivateRenderingContext(DC, RC);

  glEnable(GL_POINT_SMOOTH);
  wglSwapIntervalEXT(0);

  glEnable(GL_BLEND);
  glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  // Fell free to play arond with particle count
  // Lower numbers look great when clearing color buffer is disabled
  ParticleCount :=  1024 * 64;
  FrameDelta := 0;

  ComputeProgHandle := glCreateProgram;
  ComputeShader := glCreateShader(GL_COMPUTE_SHADER);

  with TStringList.Create do begin
  	LoadFromFile(ExtractFilePath(ParamStr(0)) + 'data\particlesystem.cs');
    ComputeShaderSrc := Text;
    Free;
  end;

  SLen := Length(ComputeShaderSrc);
  glShaderSource(ComputeShader, 1, @ComputeShaderSrc, @SLen);
  glCompileShader(ComputeShader);
  glGetShaderiv(ComputeShader, GL_COMPILE_STATUS, @Rvalue);
  if RValue = GL_FALSE then
  	begin
    	ShaderLog := GetInfoLog(ComputeShader);
			MessageDlg(ShaderLog, mtError, [mbOK], 0);
      exit;
    end;

  glAttachShader(ComputeProgHandle, ComputeShader);

  glLinkProgram(ComputeProgHandle);

  glGetProgramiv(ComputeProgHandle, GL_LINK_STATUS, @Rvalue);
  if RValue = GL_FALSE then
  	begin
    	ShaderLog := GetProgramInfoLog(ComputeProgHandle);
			MessageDlg(ShaderLog, mtError, [mbOK], 0);
      exit;
    end;

  RenderProgHandle := GenerateRenderPrograms;

	GenerateSSBO(ParticleCount);

  QueryPerformanceFrequency(QPCf);
end;

procedure TGLForm.RenderLoop;
begin
	FrameColDir := 1;
  FrameColIndex := 2;
	repeat
  	if not Pause then
  		RenderScene;
    Application.ProcessMessages;
  until Quit;
end;

procedure TGLForm.RenderScene;
var
  QPCs: Int64;
  QPCe: Int64;
  posPtr: TGLint;
begin
	// Color fade

	FrameColor[FrameColIndex] := FrameColor[FrameColIndex] + FrameDelta * 0.025 * FrameColDir;

  if FrameColDir > 0 then
  	begin
      if FrameColor[FrameColIndex] > 1 then
        begin
          FrameColor[FrameColIndex] := 1;
          if FrameColIndex < 2 then
            inc(FrameColIndex)
          else
            FrameColDir := - FrameColDir;
        end
    end
  else
  	begin
      if FrameColor[FrameColIndex] < 0 then
        begin
          FrameColor[FrameColIndex] := 0;
          if FrameColIndex > 0 then
            dec(FrameColIndex)
          else
            FrameColDir := - FrameColDir;
        end;
    end;

	glViewPort(0, 0, ClientWidth, ClientHeight);

  QueryPerformanceCounter(QPCs);

  if Options.ClearColorBuffer then
		glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT)
  else
		glClear(GL_DEPTH_BUFFER_BIT);

	// Run compute shader
	glUseProgram(ComputeProgHandle);
	glUniform1f(glGetUniformLocation(ComputeProgHandle, 'deltaT'), FrameDelta);
	glUniform3f(glGetUniformLocation(ComputeProgHandle, 'destPos'), MPos.x / ClientWidth * 2, MPos.y / ClientHeight *2 , 0);

	glUniform2f(glGetUniformLocation(ComputeProgHandle, 'vpDim'), 1, 1);
	glUniform1i(glGetUniformLocation(ComputeProgHandle, 'borderClamp'), Integer(Options.ParticleClamp));

	glDispatchCompute(ParticleCount div 16, 1, 1);

  // Set memory barrier on per vertex base to make sure we get what was written by the compute shaders
  glMemoryBarrier(GL_VERTEX_ATTRIB_ARRAY_BARRIER_BIT);

	glUseProgram(RenderProgHandle);

	glUniform3f(glGetUniformLocation(RenderProgHandle, 'inColor'), FrameColor[0], FrameColor[1], FrameColor[2]);


  posPtr := glGetAttribLocation(RenderProgHandle, 'pos');

  glBindBuffer(GL_ARRAY_BUFFER, SSBOPosition);
  glVertexAttribPointer(posPtr, 4, GL_FLOAT, False, 0, 0);
  glEnableVertexAttribArray(posPtr);
  glPointSize(4);
  glDrawArrays(GL_POINTS, 0, ParticleCount);

	SwapBuffers(DC);

  QueryPerformanceCounter(QPCe);

  FrameDelta := ((QPCe - QPCs) / QPCf * 1000) * 0.01;
  inc(FrameCount);
end;

procedure TGLForm.ResetBuffers(AParticleCount : Integer);
var
	BufferPointer : PGLVectorArray4f;
begin

  // Bind to SSBO
  glBindBuffer(GL_SHADER_STORAGE_BUFFER, SSBOPosition);

  // Map buffer
  BufferPointer := glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, AParticleCount * SizeOf(TGLVector4f), GL_MAP_WRITE_BIT or GL_MAP_INVALIDATE_BUFFER_BIT);

  FillPositionSSBO(BufferPointer, AParticleCount);

	glUnMapBuffer(GL_SHADER_STORAGE_BUFFER);

  // Bind buffer to target index 0
  glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, SSBOPosition);

  // Bind to SSBO
  glBindBuffer(GL_SHADER_STORAGE_BUFFER, SSBOVelocity);

  // Generate empty storage for the SSBO
  glBufferData(GL_SHADER_STORAGE_BUFFER, AParticleCount * SizeOf(TGLVector4f), nil, GL_STATIC_DRAW);

  // Map buffer
  BufferPointer := glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, AParticleCount * SizeOf(TGLVector4f), GL_MAP_WRITE_BIT or GL_MAP_INVALIDATE_BUFFER_BIT);

  FillVelocitySSBO(BufferPointer, AParticleCount);

	glUnMapBuffer(GL_SHADER_STORAGE_BUFFER);
end;

end.
