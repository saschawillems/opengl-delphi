This repository contains Delphi samples that demonstrate different, mostly cutting edge, OpenGL features

All samples are licensed under the Mozilla Public License Version 2.0 (http://opensource.org/licenses/MPL-2.0)

� 2014 by Sascha Willems - www.saschawillems.de

01 - glDebug 
Demonstrates GL_ARB_Debug_Output functionality introduced in OpenGL 4.4. Shows how to register a debug callback that is called by the gpu driver if an error is fired or if a performance problem is detected.

02 - computeShaderParticleSystem
A simple attractive based particle systems, with positions and velocity changes calculated using compute shaders rather than the CPU.